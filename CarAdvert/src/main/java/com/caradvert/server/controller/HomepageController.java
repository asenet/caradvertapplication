package com.caradvert.server.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tomasz Szymeczek
 **/
@RestController
public class HomepageController {
    @RequestMapping(name = "/index", method = RequestMethod.GET)
    public String index() {
        return "Simply REST API server application based on Springboot";
    }
}
