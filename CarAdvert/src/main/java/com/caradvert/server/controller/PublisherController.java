package com.caradvert.server.controller;

import com.caradvert.server.domain.service.IPublisherService;
import com.caradvert.server.dto.PublisherDto;
import com.caradvert.server.utils.converter.ParameterConverterUtil;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Tomasz Szymeczek
 **/
@RestController
@RequestMapping(value = "/api")
public class PublisherController {

    private final IPublisherService iPublisherService;

    public PublisherController(IPublisherService iPublisherService) {
        this.iPublisherService = iPublisherService;
    }

    @RequestMapping(value = "/publisher", method = RequestMethod.POST, consumes = "application/json")
    public void createUser(@Valid @RequestBody PublisherDto publisher) {
        iPublisherService.create(publisher);
    }

    @RequestMapping(value = "/publisher/{id}", method = RequestMethod.GET, produces = "application/json")
    public PublisherDto getUser(@PathVariable(value = "id") String id) {
        return iPublisherService.findById(ParameterConverterUtil.convertId(id));
    }

    @RequestMapping(value = "/publisher", method = RequestMethod.PUT, consumes = "application/json")
    public void updateAdvertiser(@Valid @RequestBody PublisherDto publisher) {
        iPublisherService.update(publisher);
    }

    @RequestMapping(value = "/publisher/{id}", method = RequestMethod.DELETE)
    public void deleteAdvertiser(@PathVariable(value = "id") String id) {
        iPublisherService.delete(ParameterConverterUtil.convertId(id));
    }

    @RequestMapping(value = "/publisher/all", method = RequestMethod.GET, produces = "application/json")
    public List<PublisherDto> getAllUsers() {
        return iPublisherService.findAll();
    }

}
