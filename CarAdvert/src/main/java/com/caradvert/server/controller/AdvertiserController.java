package com.caradvert.server.controller;

import com.caradvert.server.domain.service.IAdvertiserService;
import com.caradvert.server.dto.AdvertiserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Tomasz Szymeczek
 **/
@RestController
@RequestMapping(value = "/api")
public class AdvertiserController {

    private static final Logger logger = LoggerFactory.getLogger(AdvertiserController.class);
    private final IAdvertiserService advertiserService;

    public AdvertiserController(IAdvertiserService advertiserService) {
        this.advertiserService = advertiserService;
    }

    @RequestMapping(value = "/advertiser", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> createAdvertiser(@Valid @RequestBody AdvertiserDto advertiser) {
        if(isNull(advertiser)){
            logger.debug("Unable to create new advertiser.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        advertiserService.create(advertiser);
        logger.debug("Advertiser has been created.");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/advertiser/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<AdvertiserDto> getAdvertiserById(@Valid @PathVariable(value = "id") long id) {
        AdvertiserDto toReturn = advertiserService.findById(id);;
        if(isNull(toReturn)){
            logger.debug("Cannot find advertiser with id: {}.", id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        logger.debug("Advertiser with id: {}, {}.", id, toReturn);
        return new ResponseEntity<>(toReturn, HttpStatus.OK);
    }

    @RequestMapping(value = "/advertiser", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<?> updateAdvertiser(@Valid @RequestBody AdvertiserDto advertiser) {
        if(isNull(advertiser)){
            logger.debug("Unable to update advertiser.");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        advertiserService.update(advertiser);
        logger.debug("Advertiser has been updated.");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/advertiser/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAdvertiser(@Valid @PathVariable(value = "id") long id) {
        AdvertiserDto advertiser = advertiserService.findById(id);
        if(isNull(advertiser)){
            logger.debug("Unable to update advertiser.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        advertiserService.delete(id);
        logger.debug("Advertiser with id: {} has been deleted.", id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/advertiser/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<AdvertiserDto>> getAllAdvertisers() {
        List<AdvertiserDto> advertisers = advertiserService.findAll();
        if(isEmpty(advertisers)){
            logger.debug("There are not any advertisers.");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        logger.debug("Advertisers found: {}.", advertisers);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private <E> boolean isNull(E e){
        return e == null;
    }

    private <E> boolean isEmpty(List<E> list){
        return list.isEmpty();
    }
}