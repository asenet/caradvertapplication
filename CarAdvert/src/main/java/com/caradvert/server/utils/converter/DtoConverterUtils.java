package com.caradvert.server.utils.converter;

import com.caradvert.server.domain.model.entity.Advertiser;
import com.caradvert.server.domain.model.entity.Publisher;
import com.caradvert.server.dto.AdvertiserDto;
import com.caradvert.server.dto.PublisherDto;

/**
 * @author Tomasz Szymeczek
 **/
public class DtoConverterUtils {

    public static Advertiser fromDto(AdvertiserDto advertiserDto) {
        return new Advertiser(advertiserDto.getId(), advertiserDto.getName(), advertiserDto.getCampaigns());
    }

    public static AdvertiserDto toDto(Advertiser advertiser) {
        return new AdvertiserDto(advertiser.getId(), advertiser.getName(), advertiser.getCampaigns());
    }

    public static Publisher fromDto(PublisherDto publisherDto){
        return new Publisher(publisherDto.getId(), publisherDto.getName(), publisherDto.getCampaigns(), publisherDto.getAccount());
    }

    public static PublisherDto toDto(Publisher publisher){
        return new PublisherDto(publisher.getId(), publisher.getName(), publisher.getCampaigns(), publisher.getAccount());
    }
}
