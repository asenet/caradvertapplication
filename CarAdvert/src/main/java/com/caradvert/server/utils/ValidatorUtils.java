package com.caradvert.server.utils;

import com.caradvert.server.dto.CommonDto;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ValidatorUtils {

    public static <E> void validate(CommonDto commonDto, Logger logger, HttpStatus httpStatus, String message, E param){
        if(check(commonDto)){
            logger.debug(message, param);
            new ResponseEntity(httpStatus);
        }
    }

    private static <E> boolean check(E e){
        return e == null;
    }

    private static <E> boolean isEmpty(List<E> list){
        return list.isEmpty();
    }
}
