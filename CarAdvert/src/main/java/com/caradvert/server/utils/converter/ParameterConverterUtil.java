package com.caradvert.server.utils.converter;

/**
 * @author Tomasz Szymeczek
 **/
public class ParameterConverterUtil {

    public static Long convertId(String id) {
        Long toConvert = -1L;
        try {
            toConvert = Long.valueOf(id);
        } catch (NumberFormatException e) {
            e.getMessage();
        }
        return toConvert;
    }
}
