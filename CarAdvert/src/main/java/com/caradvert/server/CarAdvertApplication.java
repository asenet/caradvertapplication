package com.caradvert.server;

import com.caradvert.server.config.ExampleDataGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Tomasz Szymeczek
 **/
@SpringBootApplication
public class CarAdvertApplication {
    public static void main(String[] args) {
        SpringApplication.run(CarAdvertApplication.class, args);
        ExampleDataGenerator.run();
    }
}
