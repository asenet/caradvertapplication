package com.caradvert.server.config;

import com.caradvert.server.domain.service.IAdvertiserService;
import com.caradvert.server.domain.service.IPublisherService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Tomasz Szymeczek
 **/
public class ExampleDataGenerator {

    @Autowired
    private IPublisherService iPublisherService;

    @Autowired
    private IAdvertiserService iAdvertiserService;

    // method generate example data for preview
    public static void run() {

    }

}
