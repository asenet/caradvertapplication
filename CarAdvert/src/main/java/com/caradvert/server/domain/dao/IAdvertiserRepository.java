package com.caradvert.server.domain.dao;

import com.caradvert.server.domain.model.entity.Advertiser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Tomasz Szymeczek
 **/
@Repository
public interface IAdvertiserRepository extends JpaRepository<Advertiser, Long> {
}
