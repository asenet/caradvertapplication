package com.caradvert.server.domain.model.entity;

import lombok.Data;

/**
 * @author Tomasz Szymeczek
 **/
@Data
public abstract class AbstractEntityModel {

    protected long id;
    protected String name;

    public AbstractEntityModel(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public AbstractEntityModel() {
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
