package com.caradvert.server.domain.service;

import com.caradvert.server.domain.dao.IPublisherRepository;
import com.caradvert.server.domain.model.entity.Publisher;
import com.caradvert.server.dto.PublisherDto;
import com.caradvert.server.utils.converter.DtoConverterUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tomasz Szymeczek
 **/
@Service
public class PublisherService implements IPublisherService {

    private final IPublisherRepository iPublisherRepository;

    public PublisherService(IPublisherRepository iPublisherRepository) {
        this.iPublisherRepository = iPublisherRepository;
    }

    @Override
    public void create(PublisherDto t) {
        iPublisherRepository.save(DtoConverterUtils.fromDto(t));
    }

    @Override
    public void update(PublisherDto t) {
        iPublisherRepository.save(DtoConverterUtils.fromDto(t));
    }

    @Override
    public void delete(long id) {
        iPublisherRepository.deleteById(String.valueOf(id));
    }

    @Override
    public PublisherDto findById(long id) {
        Publisher publisher = iPublisherRepository
                .findById(String.valueOf(id))
                .orElse(new Publisher());
        return DtoConverterUtils.toDto(publisher);
    }

    @Override
    public List<PublisherDto> findAll() {
        return iPublisherRepository.findAll()
                .stream()
                .map(DtoConverterUtils::toDto)
                .collect(Collectors.toList());
    }
}
