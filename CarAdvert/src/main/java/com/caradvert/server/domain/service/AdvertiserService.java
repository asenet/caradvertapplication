package com.caradvert.server.domain.service;

import com.caradvert.server.domain.dao.IAdvertiserRepository;
import com.caradvert.server.domain.model.entity.Advertiser;
import com.caradvert.server.dto.AdvertiserDto;
import com.caradvert.server.utils.converter.DtoConverterUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tomasz Szymeczek
 **/
@Service
public class AdvertiserService implements IAdvertiserService {

    private final IAdvertiserRepository iAdvertiserRepository;

    public AdvertiserService(IAdvertiserRepository iAdvertiserRepository) {
        this.iAdvertiserRepository = iAdvertiserRepository;
    }

    @Override
    public void create(AdvertiserDto t) {
        iAdvertiserRepository.save(DtoConverterUtils.fromDto(t));
    }

    @Override
    public void update(AdvertiserDto t) {
        iAdvertiserRepository.save(DtoConverterUtils.fromDto(t));
    }

    @Override
    public void delete(long id) {
        iAdvertiserRepository.deleteById(id);
    }

    @Override
    public AdvertiserDto findById(long id) {
        Advertiser advertiser = iAdvertiserRepository.findById(id)
                .orElse(null);
        return advertiser != null ? DtoConverterUtils.toDto(advertiser) : null;
    }

    @Override
    public List<AdvertiserDto> findAll() {
        return iAdvertiserRepository.findAll()
                .stream()
                .map(DtoConverterUtils::toDto)
                .collect(Collectors.toList());
    }
}
