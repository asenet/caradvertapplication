package com.caradvert.server.domain.service;

import java.util.List;

/**
 * @author Tomasz Szymeczek
 **/
public interface IAbstractCommonService<E> {
    void create(E t);

    void update(E t);

    void delete(long id);

    E findById(long id);

    List<E> findAll();
}
