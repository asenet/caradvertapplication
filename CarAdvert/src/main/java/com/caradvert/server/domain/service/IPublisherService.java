package com.caradvert.server.domain.service;

import com.caradvert.server.dto.PublisherDto;

/**
 * @author Tomasz Szymeczek
 **/
public interface IPublisherService extends IAbstractCommonService<PublisherDto> {
}
