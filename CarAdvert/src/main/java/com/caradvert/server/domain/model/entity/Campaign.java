package com.caradvert.server.domain.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tomasz Szymeczek
 **/
@Data
@NoArgsConstructor
@Entity
@Table(name = "Campaign")
public class Campaign extends AbstractEntityModel {

    private Advertiser advertiser;
    private Set<Publisher> publishers;

    public Campaign(Advertiser advertiser) {
        this.advertiser = advertiser;
        this.publishers = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    @NotBlank
    @Size(max = 100)
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    @ManyToOne
    @JoinColumn(name = "advertiser_id")
    public Advertiser getAdvertiser() {
        return advertiser;
    }

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "Campaign_Publisher",
            joinColumns = {@JoinColumn(name = "campaign_id")},
            inverseJoinColumns = {@JoinColumn(name = "publisher_id")}
    )
    public Set<Publisher> getPublishers() {
        return publishers;
    }
}
