package com.caradvert.server.domain.service;

import com.caradvert.server.dto.AdvertiserDto;

/**
 * @author Tomasz Szymeczek
 **/
public interface IAdvertiserService extends IAbstractCommonService<AdvertiserDto> {
}
