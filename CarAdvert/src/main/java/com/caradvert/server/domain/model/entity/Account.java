package com.caradvert.server.domain.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Tomasz Szymeczek
 **/
@Data
@Entity
@Table(name = "Account")
public class Account {

    private long id;
    private long distance = 0;
    private BigDecimal balance = new BigDecimal(0);

    public Account() {
    }

    public Account(long distance, BigDecimal balance) {
        this.distance = distance;
        this.balance = balance;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    @Column(name = "DISTANCE")
    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

    @Column(name = "CASH_BALANCE")
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
