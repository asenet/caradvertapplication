package com.caradvert.server.domain.model.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * @author Tomasz Szymeczek
 **/
@Data
@Entity
@Table(name = "Advertiser")
public class Advertiser extends AbstractEntityModel {

    private Set<Campaign> campaigns;

    public Advertiser(long id, String name, Set<Campaign> campaigns) {
        super(id, name);
        this.campaigns = campaigns;
    }

    public Advertiser() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    @NotBlank
    @Size(max = 100)
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    @OneToMany(mappedBy = "advertiser")
    public Set<Campaign> getCampaigns() {
        return campaigns;
    }

}
