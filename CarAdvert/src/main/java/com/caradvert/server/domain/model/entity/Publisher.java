package com.caradvert.server.domain.model.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tomasz Szymeczek
 **/
@Data
@Entity
@Table(name = "Publisher")
public class Publisher extends AbstractEntityModel {

    private Set<Campaign> campaigns;
    private Account account;

    public Publisher() {
        this.account = new Account();
        this.campaigns = new HashSet<>();
    }

    public Publisher(long id, String name, Set<Campaign> campaigns, Account account) {
        this.id = id;
        this.name = name;
        this.account = account;
        this.campaigns = campaigns;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    @NotBlank
    @Size(max = 100)
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    @OneToOne
    public Account getAccount() {
        return account;
    }

    @ManyToMany(mappedBy = "publishers")
    public Set<Campaign> getCampaigns() {
        return campaigns;
    }
}
