package com.caradvert.server.domain.dao;

import com.caradvert.server.domain.model.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Tomasz Szymeczek
 **/
@Repository
public interface IPublisherRepository extends JpaRepository<Publisher, String> {
}
