package com.caradvert.server.dto;

import com.caradvert.server.domain.model.entity.Account;
import com.caradvert.server.domain.model.entity.Campaign;

import java.util.Set;

/**
 * @author Tomasz Szymeczek
 **/
public class PublisherDto implements CommonDto{

    private long id;
    private String name;
    private Set<Campaign> campaigns;
    private Account account;

    public PublisherDto(long id, String name, Set<Campaign> campaigns, Account account) {
        this.id = id;
        this.name = name;
        this.campaigns = campaigns;
        this.account = account;
    }

    public PublisherDto(){
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Campaign> getCampaigns() {
        return campaigns;
    }

    public Account getAccount() {
        return account;
    }
}
