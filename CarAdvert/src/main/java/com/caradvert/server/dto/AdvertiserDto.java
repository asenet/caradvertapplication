package com.caradvert.server.dto;

import com.caradvert.server.domain.model.entity.Campaign;

import java.util.Set;

/**
 * @author Tomasz Szymeczek
 **/
public class AdvertiserDto implements CommonDto{

    private long id;
    private String name;
    private Set<Campaign> campaigns;

    public AdvertiserDto(long id, String name, Set<Campaign> campaigns) {
        this.id = id;
        this.name = name;
        this.campaigns = campaigns;
    }

    public AdvertiserDto() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Campaign> getCampaigns() {
        return campaigns;
    }

}
